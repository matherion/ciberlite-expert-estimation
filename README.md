# CIBERlite - Expert Estimation

The rendered version of the R Markdown script for this study is hosted by GitLab pages at https://matherion.gitlab.io/ciberlite-expert-estimation. The Open Science Framework repo for this study is at https://osf.io/rwvsx/.
